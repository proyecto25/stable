# Programación y análisis de datos. 
El siguiente proyecto consiste en un programa que va orientado al análisis de datos y programación del mismo.
Se nos presenta un archivo de texto o Dataset referente a "El Centro de Política y Promoción de la Nutrición del USDA"
Donde nuestro principal objetivo es responder una serie de preguntas, con datos obtenidos del Dataset.


# Historia:
Programación y análisis de datos. 
Fue escrito y desarrollado en conjunto por los autores Jorge Carrillo Silva, Yostin Sepúlveda Caniqueo y José Valenzuela Soto, utilizando para ello el lenguaje de programación Python, específicamente la versión 3.8.5
Este programa se guía con énfasis bajo las indicaciones de la PEP-8 para el código en su totalidad, como es el respetar la longitud de cada línea, indentar correctamente, entre otras.
Contiene dos librerías, las cuales son:
Matplot.lib y Pandas.lib


# Para empezar:
Es requisito tener instalado Python3, de preferencia la versión 3.8.5 y está pensada que su ejecución sea si o sí en un entorno Linux, tales como Debian y sus derivados, etc.
También es necesario tener instaladas las librería Matplot.lib y Pandas.lib


# Instalación:
Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "Programación y análisis de datos" en el directorio de su preferencia, el enlace HTTPS es https://gitlab.com/proyecto25/stable.git.

2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "Proyecto 2" que contiene 4 archivos, es esencial que pueda comprobar y cerciorarse que se encuentran los paquetes y librerías esenciales, ya que de lo contrario el programa podría no funcionar adecuadamente (Dataset.py, suministro_alimentos_kcal.csv, proyecto2.pdf y README.md)

3- Posteriormente, ya está usted preparado para ejecutar el programa. Para ello, abrirá una terminal (En caso de estar en Linux) y en el directorio que tenga los archivos, ejecutará el siguiente comando.
python3 Dataset.py


# Codificación:
El programa fue construido y soporta la codificación UTF-8.


# Construido con:
Atom: Uno de los 2 IDE's utilizados para desarrollar el proyecto.
VSCodium: Junto con Atom, es uno de los Entornos de desarrollo utilizados para el desarrollo del proyecto.


# Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3. 


# Autores y creadores del proyecto:
Jorge Carrillo Silva, Yostin Sepulveda Caniqueo y José Valenzuela Soto.